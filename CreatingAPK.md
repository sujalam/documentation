
* Make sure your Android Device or Phone is in Developer mode 
    * Go To *Settings*
        * *About Phone*
            * *Software*
                * **Tap on 'Build Number' 7 times quickly**
        * Select *Developer Options* (this is still under *Settings*)
            * **Enable** *Stay Awake and USB Debugging*
* Connect your phone to your computer and make sure to *Always Allow USB Debugging* when your phone connects
* Download Virtualbox from  https://www.virtualbox.org 
* Download an Ubuntu image https://ubuntu.com/download/desktop
* Set up a new virtual machine in Virtualbox using the image from step 2
    * Allocate atleast **2048 MB** of memory
    * Allocate atleast **20 GB** of disk space
* Install Ubuntu on the virtual machine
* Open the *Terminal* in your virtual machine and run the following commands:  

    **user@user~VirtualBox:~$** *sudo apt install git*

    **user@user~VirtualBox:~$** *git clone https://github.com/kivy/buildozer.git* 

    **user@user~VirtualBox:~$** *cd buildozer*  

    **user@user~VirtualBox:~$** *sudo apt-get install python3.6*  

    **user@user~VirtualBox:~$** *sudo apt-get install -y python3-setuptools*

    **user@user~VirtualBox:~$** *sudo python3 setup.py install*

    **user@user~VirtualBox:~$** *cd ..*

    **user@user~VirtualBox:~$** *git clone https://gitlab.com:sujalam/frontend-app.git*

    **user@user~VirtualBox:~$** *cd frontend-app*

    **user@user~VirtualBox:~$** *sudo apt update*

    **user@user~VirtualBox:~$** *sudo apt install -y git zip unzip openjdk-8-jdk python3-pip autoconf libtool pkg-config zlib1g-dev libncurses5-dev libncursesw5-dev libtinfo5 cmake libffi-dev libssl-dev*

    **user@user~VirtualBox:~$** *pip3 install --user --upgrade cython virtualenv*

    **user@user~VirtualBox:~$** *sudo apt-get install cython*

    **user@user~VirtualBox:~$** *buildozer android debug deploy run*





